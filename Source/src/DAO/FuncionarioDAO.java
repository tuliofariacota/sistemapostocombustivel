/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Cliente;
import Model.Funcionario;
import View.ViewCliente;
import View.ViewFuncionario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tulio
 */
public class FuncionarioDAO implements InterfaceDAO {

    private String sql;
    private Connection conn;
    private PreparedStatement ps = null;

    @Override
    public void adiciona(Object obj) {
        try {
            Cliente cliente = (Cliente) obj;
            ps = Persistencia.conexao().prepareStatement("INSERT INTO cliente (nome, cpf, senha, email) VALUES (?,?,?,?)");
            ps.setString(1, cliente.getNome());
            ps.setString(2, cliente.getCpf());
            ps.setString(3, cliente.getSenha());
            ps.setString(4, cliente.getEmail());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ViewCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void exclui(Object obj) {
        try {
            Cliente cliente = (Cliente) obj;
            ps = Persistencia.conexao().prepareStatement("DELETE FROM aluno WHERE cpf = ?");
            ps.setString(1, cliente.getCpf());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ViewCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void altera(Object obj) {
        try {
            Cliente cliente = (Cliente) obj;
            ps = Persistencia.conexao().prepareStatement("UPDATE aluno SET nome = ?, cpf = ? WHERE idAluno = ?");
            ps.setString(1, cliente.getNome());
            ps.setString(2, cliente.getCpf());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ViewCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public ArrayList<Object> consulta(Object obj, String sql) {
        try {
            Funcionario funcionario = (Funcionario) obj;
            ResultSet rs = null;
            ps = Persistencia.conexao().prepareStatement(sql);
            rs = ps.executeQuery();
            ArrayList<Object> resultado = new ArrayList<>();
            while (rs.next()) {
                //resultado.add(rs.getString("idFuncionario") + ";" + rs.getString("nome") + ";" + rs.getString("turno") + ";" + rs.getString("senha") + ";" + rs.getString("email") + ";" + rs.getString("cpf") + ";" + rs.getString("idCargo") + ";" + rs.getString("nivelPermissao"));
                resultado.add(rs.getString("nome"));
            }
            return resultado;
        } catch (SQLException ex) {
        }
        return null;
    }

    public Boolean login(Object obj) {
        try {
            Funcionario funcionario = (Funcionario) obj;
            ResultSet rs = null;
            ps = Persistencia.conexao().prepareStatement("SELECT COUNT(*) AS total FROM funcionario WHERE email = ? AND senha = ?");
            ps.setString(1, funcionario.getEmail());
            ps.setString(2, funcionario.getSenha());
            rs = ps.executeQuery();
            int total = 0;
            while (rs.next()) {
                total += rs.getInt("total");
            }
            return total > 0;
        } catch (SQLException ex) {
            Logger.getLogger(ViewFuncionario.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

}
