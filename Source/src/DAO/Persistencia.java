/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author tulio
 */
public class Persistencia {
    private static Connection conn = null;
    private final String HOST = "localhost:3306/lpstrabalho";
    private final String USER = "root";
    private final String PASSWORD = "";
    
    private Persistencia() throws SQLException{
        try{
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://" + HOST, USER, PASSWORD);
        }catch(ClassNotFoundException e){
            System.out.println("Não foi possivel encontrar o Drive");
        }catch(SQLException ex){
            System.out.println("Não foi possivel conectar ao banco de dados");
        }
    }
    
    public static Connection conexao() throws SQLException{
        if(conn == null)
            new Persistencia();

        return conn;
    }
    
}
